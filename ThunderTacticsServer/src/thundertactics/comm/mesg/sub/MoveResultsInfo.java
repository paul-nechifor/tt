package thundertactics.comm.mesg.sub;

import java.util.List;
import thundertactics.logic.fight.MoveResults;

public class MoveResultsInfo {
    public List<MoveResults.Damage> damages;
}

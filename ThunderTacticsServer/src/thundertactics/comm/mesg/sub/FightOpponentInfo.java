package thundertactics.comm.mesg.sub;

public class FightOpponentInfo {
    public String name;
    public UnitInfo[] battleUnits;
    public String appearance;

    // Stats.
    public int level;
    public int defense;
    public int damage;
    public int leadership;
    public int gold;
}

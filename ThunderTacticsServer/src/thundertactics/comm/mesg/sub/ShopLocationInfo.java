package thundertactics.comm.mesg.sub;

public class ShopLocationInfo {
	/**
	 * Shop id
	 */
	public int i;
    public String name;
    public float x;
    public float y;
    public float rotation;
}

package thundertactics.comm.mesg.sub;

public class UnitInfo {
    public long type;
    public int damageDelta;
    public int defenseDelta;
    public int moveRangeDelta;
    public int attackRangeDelta;
    public int initiativeDelta;
    public int count;
}

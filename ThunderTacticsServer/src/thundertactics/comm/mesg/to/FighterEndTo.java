package thundertactics.comm.mesg.to;

import thundertactics.comm.mesg.sub.PlayerInfo;

public class FighterEndTo extends MesgTo {
    public String fighter;
    public int type;
    public String kickedReason;
    public PlayerInfo playerInfo;
}

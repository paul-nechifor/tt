package thundertactics.comm.mesg.to;

public final class ChatTo extends MesgTo {
    public String from;
    public String text;
}

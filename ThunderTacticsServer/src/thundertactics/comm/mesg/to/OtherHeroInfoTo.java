package thundertactics.comm.mesg.to;

public final class OtherHeroInfoTo extends MesgTo {
	/**
	 * ID of the hero
	 */
	public int i;
    public String name;
    public String appearance;
    public int level;
}

package thundertactics.comm.mesg.to.playerInfo;

import thundertactics.comm.mesg.sub.ItemInfo;

public class InventoryTo {
	/**
	 * Inventory - contains items non equipped
	 */
    public ItemInfo[] i;
    /**
     * Weared items - contains items equipped
     */
    public ItemInfo[] w;
}

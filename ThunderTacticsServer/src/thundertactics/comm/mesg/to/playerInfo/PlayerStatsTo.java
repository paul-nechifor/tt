package thundertactics.comm.mesg.to.playerInfo;

/**
 * Contains info about stats for player's hero
 * Shorten names for transfer optimization
 * @author Tiby
 *
 */
public class PlayerStatsTo {
	/**
	 * Level
	 */
    public int l;
    /**
     * Defense
     */
    public int d;
    /**
     * Damage
     */
    public int da;
    /**
     * Leadership
     */
    public int le;
    /**
     * Gold
     */
    public int g;
    /**
     * AddPoints
     */
	public int a;
	/**
	 * Experience
	 */
	public double e;
	/**
	 * MaxLife
	 */
	public int m;
	/**
	 * Life
	 */
	public int li;
}

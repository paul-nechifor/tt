package thundertactics.comm.mesg.to;

public final class FighterSaidTo extends MesgTo {
    public String from;
    public String text;
}

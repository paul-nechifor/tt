package thundertactics.comm.mesg.to;

public final class NearChatTo extends MesgTo {
    public String from;
    public String text;
}

package thundertactics.comm.mesg.to;

public final class ServerMessageTo extends MesgTo {
    public String text;
    public boolean error;
}

package thundertactics.comm.mesg.from;

public class SellUnitFrom extends MesgFrom {
	/**
	 * Count of units to sell
	 */
	public int c;
	/**
	 * Location of unit in inventory
	 */
	public int f;
}

package thundertactics.comm.mesg.from;

public class BuyItemFrom extends MesgFrom{
	/** 
	 * Shop id
	 */
	public int s;
	/**
	 * Item id
	 */
	public int i;
	/**
	 * Count
	 */
	public int c;
}

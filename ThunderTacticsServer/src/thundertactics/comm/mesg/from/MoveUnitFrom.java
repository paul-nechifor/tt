package thundertactics.comm.mesg.from;

public class MoveUnitFrom extends MesgFrom{
	/**
	 * Count of units to move
	 */
	public int c;
	/**
	 * From where to move
	 */
	public int f;
	/**
	 * Where to move
	 */
	public int t;
}

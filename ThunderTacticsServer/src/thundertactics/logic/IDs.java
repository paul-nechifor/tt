package thundertactics.logic;

public class IDs {
	// 1 - 1000 -> units
	public final static int ARCHER = 1;
	public final static int DARKKNIGHT = 2;
	public final static int GUARDSMAN = 3;
	public final static int KNIGHT = 4;
	public final static int LONGBOWMAN = 5;
	
	
	// 1001 - 1500 -> scrolls
	public static final int CROWNOFLEADER = 1001;
	public static final int SROLLOFDECIMATION = 1002;
	public static final int SCROLLOFDIVINEINTERVENTION = 1003;
	// 1501 - 2000
	public static final int POTIONOFLIFE = 1501;
	
	// 2001 - 3000 -> items
	public static final int SMALLSHIELD = 2001;
	public static final int SMALLBOW = 2002;
	public static final int SMALLSWORD = 2003;
	public static final int SCYTHE = 2004;
	public static final int BIGSHIELD = 2005;
	public static final int VIKINGHELMET = 2006;
}

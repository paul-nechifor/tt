package thundertactics.logic.fight;

/**
 * An object that can be present on a cell in a FightScene, the most common
 * example is a Unit.
 */
public interface SceneObject {
}
